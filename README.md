USING SNAP TO MESS WITH AN SVG
==============================

Snap: [http://snapsvg.io/](http://snapsvg.io/)

"Snap.svg is a brand new JavaScript library for working with SVG. Snap provides
web developers with a clean, streamlined, intuitive, and powerful API for
animating and manipulating both existing SVG content, and SVG content generated
with Snap."

Snap's best feature (IMHO) is that it can work on existing SVG content - you don't
have to create the SVG object using Snap to be able to work on it. Pretty cool.

TfL TUBE MAP
------------

TfL: [https://tfl.gov.uk/](https://tfl.gov.uk/)

TfL don't provide direct links (that I could find) to download the SVG version
of their famous map, but if you dig around on their website you can snag a
copy out of the source. That's what I did. See the <svg> tag in the HTML file.

Apparently there are some licencing rules:

[https://tfl.gov.uk/info-for/suppliers-and-contractors/using-the-maps](https://tfl.gov.uk/info-for/suppliers-and-contractors/using-the-maps)

I don't know how aggressively TfL police that kind of thing.

DEMO
----

I have a project in mind that will use the TfL map and manipulate
elements of it. This demo doesn't do anything useful other than to show how
to manipulate SVG elements. This is **not** production code. I'm really only
putting it here so that I can find it again later.

Live: [http://ottaky.com/svg_map/](http://ottaky.com/svg_map/)
